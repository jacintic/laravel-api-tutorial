<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Models\User;
use App\Models\Product;
use App\Http\Resources\UserCollection;
use App\Http\Resources\ProductCollection;
use App\Http\Controllers\ProductController;
use App\Http\Resources\ProductResource;
use App\Http\Resources\UserResource;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});


////////////////////
////// users ///////
////////////////////

/*routes/api.php
* Obtenemos todos los usuarios accediendo directamente al Collection de user
*/

Route::get('/users', function () {
    return new UserCollection(User::all());
});

// User get by id
Route::get('/users/{id}', function ($id) {
    return new UserResource(USer::findOrFail($id));
});

// Users within ID range
// note: executes "idRange" a scope from Users Model
Route::get('/users/idrange/{ids}/{ide}', function ($ids,$ide) {
    return new UserCollection(User::idRange($ids,$ide)->get());
});

// create an user
/**
 * Create a new user via POST
 *
 * USER FILLABLE
 *   'name',
 *   'email',
 *   'password',
 *
 *  NOTE: email must be UNIQUE
 */
Route::post('/users', function (Request $request) {
    $users = new UserCollection(User::all());
    foreach ($users as $user) {
        if($user->email == $request->email)
            return response()->json(['Status'=> "Error: email already in the DB, must be UNIQUE!"], 400);
    }
    return new UserResource(User::create($request->all()));
});

// cambiar los datos de un usuario
//(PATCH)
Route::patch('/users/{id}', function(Request $request, $id) {
    User::findOrFail($id)->update($request->all());
    return new UserResource(User::findOrFail($id));
});

// eliminar un usuario

Route::delete('/users/{id}', function($id) {
    try
    {
        $user = User::findOrFail($id);
    }
    catch(Illuminate\Database\Eloquent\ModelNotFoundException)
    {
        return response()->json(['Status'=> "Error: user doesn't exist!"], 404);
    }
    $user->delete();
    return response()->json(['Status'=> 'Success!'], 200);
});


///////////////////
//// products /////
///////////////////

/*routes/api.php
* Llamamos al controlador que se encarga de guardar nuevos productos
*/

Route::post('/product', [ProductController::class, 'store']);

// prod get all products
Route::get('/product', function () {
    return new ProductCollection(Product::all());
});

// prod get by id
Route::get('/product/{id}', function ($id) {
    return new ProductResource(Product::findOrFail($id));
});

// MODIFY A PROD
Route::put('/product/{id}', function(Request $request, $id) {
    Product::findOrFail($id)->update($request->all());
    return new ProductResource(Product::findOrFail($id));
});


// MODIFY A PROD (PATCH)
Route::patch('/product/{id}', function(Request $request, $id) {
    Product::findOrFail($id)->update($request->all());
    return new ProductResource(Product::findOrFail($id));
});


// delete product
Route::delete('/product/{id}', function($id) {
    try
    {
        $product = Product::findOrFail($id);
    }
    catch(Illuminate\Database\Eloquent\ModelNotFoundException)
    {
        return response()->json(['Status'=> "Error: product doesn't exist!"], 404);
    }
    $product->delete();
    return response()->json(['Status'=> 'Success!'], 200);
});



