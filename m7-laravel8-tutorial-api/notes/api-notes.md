# API documentation  
  
  
## API POST a product  
  
```
## In postman
# POST
http://localhost/api/product

## json:
{
"name": "Sofa",
"desc": "Sofa for 4 people, various colors available",
"price": 1000.56
}

## in Product Model

/**
* The attributes that are mass assignable.
*
* @var array<int, string>
*/
 protected $fillable = [
    'name',
    'desc',
    'price',
];

## In product controller
    /* Creamos el metodo de guardar un nuevo producto con los datos recibidos por formulario.
    * Al preveer que es una petición de API podemos devolver el resultado en formato JSON
    */

    public function store(Request $request)
    {
        $product = Product::create($request->all());
        return response()->json($product, 200);
    }

```

## Review and documment get Users by ID range (api, model)  

## PUT  
  
```
## API

// MODIFY A PROD
Route::put('/product/{id}', function(Request $request, $id) {
    Product::findOrFail($id)->update($request->all());
    return new ProductResource(Product::findOrFail($id));
});


## JSON

{
	"id": 99,
	"name": "Tist",
	"desc": "Tost.Qui rerum eum assumenda quam officia saepe nemo. Odit vero sint aliquid aut repellendus quos. Ab non facilis id aut laudantium inventore. Minus ut iusto vero.",
	"price": 13,
	"remember_token": null,
	"created_at": "2022-02-02T15:58:19.000000Z",
	"updated_at": "2022-02-02T15:58:19.000000Z",
	"imagen": "https://via.placeholder.com/400x400.png/0099ff?text=aperiam",
	"category_id": 1
}
```
  
  
  
## Resource   
  
```
toArray($request)
{
	return [
		'id' => $this->id,
		'name' => $this->name,
		'email' => $this->email
 	]
}
```

### Resource foreign refernces/keys  
  
```
class ProductResource extends JsonResource
{
    public function toArray($request)
    {
       return [
            'id' => $this->id,
            'category' => CategoryResource::collection($this->category),
        ];
}
```

The Class that has a primary key referenced by another class must have either hasOne or HasMany.  
This must be defined in the Model.  
  
Many to Many => hasOneThrough

many to one => `return $this->belongsTo(Category::class);`
  
(documentación de Laravel de relacciones entre tablas)[https://laravel.com/docs/8.x/eloquent-relationships]  

  
  
## Resource collection  
  
```
return [
            'data' => $this->collection,
            'links' => [
                'self' => 'link-value',
            ],
        ];
  
